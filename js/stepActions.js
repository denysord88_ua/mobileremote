var stepActions = {
  "actions": [
    {
      "title": "Action",
      "inputElement": "select",
      "inputValues": [
        "custom",
        "spin",
        "shift",
        "manual_open",
        "manual_vertical_scroll",
        "manual_horizontal_scroll",
        "open_bet_editing",
        "inc_bet",
        "dec_bet"
      ],
      "afterInputTitle": ""
    },
    {
      "title": "Wait millis",
      "inputElement": "input_text",
      "inputValues": [
        "0"
      ],
      "afterInputTitle": "before step"
    },
    {
      "title": "Wait millis",
      "inputElement": "input_text",
      "inputValues": [
        "0"
      ],
      "afterInputTitle": "after step"
    },
    {
      "title": "Wait for image",
      "inputElement": "input_file",
      "inputValues": [
        ""
      ],
      "afterInputTitle": "before step"
    },
    {
      "title": "Wait for image",
      "inputElement": "input_file",
      "inputValues": [
        ""
      ],
      "afterInputTitle": "after step"
    },
    {
      "title": "Click on image",
      "inputElement": "input_file",
      "inputValues": [
        ""
      ],
      "afterInputTitle": ""
    },
    {
      "title": "Manual check",
      "inputElement": "",
      "inputValues": [
        ""
      ],
      "afterInputTitle": "before step"
    },
    {
      "title": "Manual check",
      "inputElement": "",
      "inputValues": [
        ""
      ],
      "afterInputTitle": "after step"
    },
    {
      "title": "Screen shoot",
      "inputElement": "input_checkbox",
      "inputValues": [
        "0"
      ],
      "afterInputTitle": "before step"
    },
    {
      "title": "Screen shoot",
      "inputElement": "input_checkbox",
      "inputValues": [
        "0"
      ],
      "afterInputTitle": "after pass step"
    },
    {
      "title": "Screen shoot",
      "inputElement": "input_checkbox",
      "inputValues": [
        "0"
      ],
      "afterInputTitle": "after fail step"
    },
    {
      "title": "Step data",
      "inputElement": "textarea",
      "inputValues": [
        ""
      ],
      "afterInputTitle": ""
    }
  ]
};