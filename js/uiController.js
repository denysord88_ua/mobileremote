const PATTERN_FOR_1_NUMBER_IN_ID = /([A-Z_]+)([0-9]+)([A-Z_]+)/;
const PATTERN_FOR_2_NUMBERS_IN_ID = /([A-Z_]+)([0-9]+)([A-Z_]+)([0-9]+)([A-Z_]+)/;
const PATTERN_FOR_3_NUMBERS_IN_ID = /([A-Z_]+)([0-9]+)([A-Z_]+)([0-9]+)([A-Z_]+)([0-9]+)([A-Z_]+)/;
const PATTERN_FOR_4_NUMBERS_IN_ID = /([A-Z_]+)([0-9]+)([A-Z_]+)([0-9]+)([A-Z_]+)([0-9]+)([A-Z_]+)([0-9]+)([A-Z_]+)/;
var exampleParams = {
    "host": ["wl-beta.dev.pls.life"],
    "currency": [
        "USD",
        "EUR",
        "UAH",
        "RUB",
        "CNY"
    ],
    "loginMail": ["eur@tst.ord"],
    "server": [
        "beta-ngl",
        "rc-ngl",
        "rc-ngl-platform"
    ],
    "language": [
        "en",
        "ru",
        "zh",
        "vi",
        "th",
        "de"
    ],
    "gameType": [
        "Flash",
        "Desktop",
        "UUI_Mob",
        "Old_Mob",
        "Unicore",
        "Unicore_Mob"
    ],
    "branch":[
        "test",
        "DEVCL-914",
        "PHP-136",
        "DEVCL-3298",
        "PHP-2608"
    ],
    "game": [
        "alice_c_mob",
        "aquarium_c_mob",
        "art_of_the_heist_mob",
        "down_the_pub_c_mob",
        "taiga_c_mob",
        "burlesque_c_mob"
    ]
};
window.onload = function() {
    let params = Object.keys(exampleParams);
    let selects = document.getElementsByClassName("CUSTOM_PARAMETER_SELECT");
    for(let j = 0; j < selects.length; j++) {
        for (let i = 0; i < params.length; i++) {
            let optionElem = document.createElement("option");
            optionElem.class = "PARAMETER_VALUE_TEXT_FIELD";
            optionElem.value=params[i];
            optionElem.text=params[i];
            selects[j].appendChild(optionElem);
        }
    }
}

function addCustomTestParam(customParamBtn) {
    let suiteId = customParamBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[2];
    let testId = customParamBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[4];
    let testTable = (document.getElementById("TS_" + suiteId + "_TC_" + testId + "_PARAMETERS_TABLE")).firstElementChild;
    let nextRow = 0;//nextRow = testTable.rows[testTable.rows.length - 1].id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[6];
    if(testTable.rows.length > 2) nextRow = 1 + parseInt(testTable.rows[testTable.rows.length - 2].id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[6]);
    let newRowId = "TS_" + suiteId + "_TC_" + testId + "_PARAMETERS_TABLE_" + nextRow + "_ROW";

    let childTd1 = document.createElement("td");
    childTd1.className = "TC_PARAMETERS_TABLE_COLUMN";
    childTd1.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_COLUMN_1_TD";

    let customParamsSelect = document.getElementById("TS_" + suiteId + "_TC_" + testId + "_CUSTOM_PARAMETER_SELECT");
    let selectedOption = customParamsSelect.options[customParamsSelect.selectedIndex].text;

    let paramNameInput = document.createElement("input");
    paramNameInput.type = "text";
    paramNameInput.class = "PARAMETER_NAME_TEXT_FIELD";
    paramNameInput.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_NAME_TEXT_FIELD";
    paramNameInput.value = selectedOption;
    childTd1.appendChild(paramNameInput);

    let childTd2 = document.createElement("td");
    childTd2.className = "TC_PARAMETERS_TABLE_COLUMN";
    childTd2.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_COLUMN_2_TD";

    let paramValuesByName = exampleParams[selectedOption];
    if(paramValuesByName.length == 1) {
        let paramValueInput = document.createElement("input");
        paramValueInput.type = "text";
        paramValueInput.class = "PARAMETER_VALUE_TEXT_FIELD";
        paramValueInput.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_VALUE_TEXT_FIELD";
        paramValueInput.value = paramValuesByName[0];
        childTd2.appendChild(paramValueInput);
    } else {
        let paramValueSelect = document.createElement("select");
        paramValueSelect.class = "PARAMETER_VALUE_SELECT";
        paramValueSelect.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_VALUE_SELECT";

        for(let i = 0; i < paramValuesByName.length; i++) {
            let option = document.createElement("option");
            option.class = "PARAMETER_VALUE_OPTION";
            option.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_VALUE_" + (i+1) + "_OPTION";
            option.value = paramValuesByName[i];
            option.text = paramValuesByName[i];
            paramValueSelect.appendChild(option);
        }

        childTd2.appendChild(paramValueSelect);
    }

    let childTd3 = document.createElement("td");
    childTd3.className = "TC_PARAMETERS_TABLE_COLUMN";
    childTd3.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_COLUMN_3_TD";

    childTd3.innerHTML = "<input type=\"button\" onclick=\"delTestParam(this)\" class=\"CONTROL_BUTTONS\" id=\"TS_" + suiteId + "_TC_" + testId + "_DELETE_PARAMETER_" + nextRow + "_BUTTON\" value=\"X\">";

    let childTr = document.createElement("tr");
    childTr.className = "TC_PARAMETERS_TABLE_ROW";
    childTr.id = newRowId;
    childTr.appendChild(childTd1);
    childTr.appendChild(childTd2);
    childTr.appendChild(childTd3);
    testTable.insertBefore(childTr, testTable.rows[testTable.rows.length - 1]);
}

function addNewTestParam(addParamBtn) {
    let suiteId = addParamBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[2];
    let testId = addParamBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[4];
    let testTable = (document.getElementById("TS_" + suiteId + "_TC_" + testId + "_PARAMETERS_TABLE")).firstElementChild;
    let nextRow = 0;//nextRow = testTable.rows[testTable.rows.length - 1].id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[6];
    if(testTable.rows.length > 2) nextRow = 1 + parseInt(testTable.rows[testTable.rows.length - 2].id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[6]);
    let newRowId = "TS_" + suiteId + "_TC_" + testId + "_PARAMETERS_TABLE_" + nextRow + "_ROW";

    let childTd1 = document.createElement("td");
    childTd1.className = "TC_PARAMETERS_TABLE_COLUMN";
    childTd1.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_COLUMN_1_TD";

    let paramNameInput = document.createElement("input");
    paramNameInput.type = "text";
    paramNameInput.class = "PARAMETER_NAME_TEXT_FIELD";
    paramNameInput.id="TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_NAME_TEXT_FIELD";
    childTd1.appendChild(paramNameInput);

    let childTd2 = document.createElement("td");
    childTd2.className = "TC_PARAMETERS_TABLE_COLUMN";
    childTd2.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_COLUMN_2_TD";

    let paramValueInput = document.createElement("input");
    paramValueInput.type = "text";
    paramValueInput.class = "PARAMETER_VALUE_TEXT_FIELD";
    paramValueInput.id="TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_VALUE_TEXT_FIELD";
    childTd2.appendChild(paramValueInput);

    let childTd3 = document.createElement("td");
    childTd3.className = "TC_PARAMETERS_TABLE_COLUMN";
    childTd3.id = "TS_" + suiteId + "_TC_" + testId + "_PARAMETER_" + nextRow + "_COLUMN_3_TD";

    childTd3.innerHTML = "<input type=\"button\" onclick=\"delTestParam(this)\" class=\"CONTROL_BUTTONS\" id=\"TS_" + suiteId + "_TC_" + testId + "_DELETE_PARAMETER_" + nextRow + "_BUTTON\" value=\"X\">";

    let childTr = document.createElement("tr");
    childTr.className = "TC_PARAMETERS_TABLE_ROW";
    childTr.id = newRowId;
    childTr.appendChild(childTd1);
    childTr.appendChild(childTd2);
    childTr.appendChild(childTd3);
    testTable.insertBefore(childTr, testTable.rows[testTable.rows.length - 1]);
}

function addTestStep(addStepBtn) {
    //TS_1_TC_1_ADD_STEP_TITLE
    let suiteId = addStepBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[2];
    let testId = addStepBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[4];
    let testStepsContainer = document.getElementById("TS_" + suiteId + "_TC_" + testId + "_SCENARIO_CONTAINER");
    let currentSteps = document.getElementById("TS_" + suiteId + "_TC_" + testId + "_SCENARIO_CONTAINER").getElementsByTagName("fieldset");
    let nextStepId = 0;
    if(currentSteps.length != 0) nextStepId = 1 + (currentSteps[currentSteps.length - 1]).id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[6];



    var test = "";

}

function changeTestSuiteView(testSuiteBtn) {
    let suiteId = testSuiteBtn.id.match(PATTERN_FOR_1_NUMBER_IN_ID)[2];
    if (testSuiteBtn.value == "+") {
        testSuiteBtn.value = "-";
        showElementById("TS_" + suiteId + "_CONTAINER");
    } else {
        testSuiteBtn.value = "+";
        hideElementById("TS_" + suiteId + "_CONTAINER");
    }
}

function changeTestView(testBtn) {
    let suiteId = testBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[2];
    let testId = testBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[4];
    let testIdTxt = "TS_" + suiteId + "_TEST_" + testId + "_CONTAINER";
    if (testBtn.value == "+") {
        testBtn.value = "-";
        showElementById(testIdTxt);
    } else {
        testBtn.value = "+";
        hideElementById(testIdTxt);
    }
}

function changeTestParamsView(paramsBtn) {
    let suiteId = paramsBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[2];
    let testId = paramsBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[4];
    let paramsId = "TS_" + suiteId + "_TEST_" + testId + "_PARAMS_CONTAINER";
    if (paramsBtn.value == "+") {
        paramsBtn.value = "-";
        showElementById(paramsId);
    } else {
        paramsBtn.value = "+";
        hideElementById(paramsId);
    }
}

function changeTestScenarioView(scenarioBtn) {
    let suiteId = scenarioBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[2];
    let testId = scenarioBtn.id.match(PATTERN_FOR_2_NUMBERS_IN_ID)[4];
    let scenarioId = "TS_" + suiteId + "_TEST_" + testId + "_SCENARIO_CONTAINER";
    if (scenarioBtn.value == "+") {
        scenarioBtn.value = "-";
        showElementById(scenarioId);
    } else {
        scenarioBtn.value = "+";
        hideElementById(scenarioId);
    }
}

function changeTestStepView(stepBtn) {
    let suiteId = stepBtn.id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[2];
    let testId = stepBtn.id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[4];
    let stepId = stepBtn.id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[6];
    let stepIdTxt = "TS_" + suiteId + "_TEST_" + testId + "_STEP_" + stepId + "_CONTAINER";
    if (stepBtn.value == "+") {
        stepBtn.value = "-";
        showElementById(stepIdTxt);
    } else {
        stepBtn.value = "+";
        hideElementById(stepIdTxt);
    }
}

function delTestParam(paramBtn) {
    let suiteId = paramBtn.id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[2];
    let testId = paramBtn.id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[4];
    let paramId = paramBtn.id.match(PATTERN_FOR_3_NUMBERS_IN_ID)[6];
    document.getElementById("TS_" + suiteId + "_TC_" + testId + "_PARAMETERS_TABLE_" + paramId + "_ROW").remove();
}

function delTestStep(delStepBtn) {
    delStepBtn.parentElement.parentElement.remove();
}

function hideElementById(elementId) {
    document.getElementById(elementId).hidden = true;
}

function showElementById(elementId) {
    document.getElementById(elementId).hidden = false;
}

function add() {
    var container = document.getElementsByClassName("CONTAINER")[0];
    var childDoc = document.createElement("div");
    childDoc.className = "div";
    childDoc.innerHTML = "myDiv";
    container.appendChild(childDoc);
}

function send() {
    var xhr = new XMLHttpRequest();
    var url = "/getjson";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = JSON.parse(xhr.responseText);
            console.log(json.email + ", " + json.password);
        }
    };
    var data = JSON.stringify({"email": "hey@mail.com", "password": "101010"});
    xhr.send(data);
}

